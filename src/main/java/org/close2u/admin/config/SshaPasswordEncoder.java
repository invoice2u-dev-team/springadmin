package org.close2u.admin.config;

import com.unboundid.util.Base64;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.ParseException;
import java.util.Arrays;

@Configuration
public class SshaPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        // Genera una sal aleatoria
        byte[] salt = new byte[8];
        new SecureRandom().nextBytes(salt);

        // Aplica el algoritmo SHA-1
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(rawPassword.toString().getBytes());
            md.update(salt);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // Combina la sal y el hash, y codifícalos en base64
        byte[] hash = md.digest();
        byte[] hashPlusSalt = new byte[hash.length + salt.length];
        System.arraycopy(hash, 0, hashPlusSalt, 0, hash.length);
        System.arraycopy(salt, 0, hashPlusSalt, hash.length, salt.length);

        return "{SSHA}" + new String(Base64.encode(hashPlusSalt));
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        // Extrae la sal y el hash de la contraseña almacenada
        byte[] storedPassword = new byte[0];
        try {
            storedPassword = Base64.decode(encodedPassword.substring(6));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        byte[] salt = Arrays.copyOfRange(storedPassword, 20, storedPassword.length);

        // Aplica el algoritmo SHA-1 para verificar la contraseña
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(rawPassword.toString().getBytes());
            md.update(salt);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        byte[] checkHash = md.digest();

        // Compara el hash calculado con el almacenado
        byte[] storedHash = Arrays.copyOf(storedPassword, 20);
        return Arrays.equals(checkHash, storedHash);
    }

}
