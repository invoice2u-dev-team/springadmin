package org.close2u.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@EnableAdminServer
@EnableAutoConfiguration
public class AdminApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        new SpringApplication(AdminApplication.class).run(args);
    }
}
